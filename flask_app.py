#!flask/bin/python
from datetime import date, timedelta
from flask import Flask
from flask import request

import mixpanel
import cyfe

app = Flask(__name__)

def get_period(start, end):
    today = date.today()
    startdate = today - timedelta(start)
    enddate = today - timedelta(end)

    return (str(startdate), str(enddate))

@app.route('/mixpanel/api/v1.0/<event>/<apisecret>/<apikey>', methods=['GET'])
def get_event(event, apisecret, apikey):
    api = mixpanel.API(api_key = apikey, api_secret = apisecret)
    period = get_period(30, 1)
    event = event.replace('_', ' ')
    by = request.args.get('by', 'None')
    charttype = request.args.get('charttype', 'None')

    if by != None and by != '' and by != 'none':
        by = by.replace('_', ' ')
        data = cyfe.get_segmentation(api, event, by, period[0], period[1])
    else:
        data = cyfe.get_event(api, event, period[0], period[1])

    clean = request.args.get('clean')

    return cyfe.mixpaneldata_to_piecyfe(data, clean) if charttype=='pie' else cyfe.mixpaneldata_to_cyfe(data, clean)

# Uncomment this line to run in local
#app.run(debug=True)
