def get_event(api, eventName, from_date, to_date):
    return api.request(['events'], {
    'event' : [eventName,],
    'type' : 'general',
    'unit' : 'day',
    'interval' : 10,
    'from_date': from_date,
    'to_date': to_date,
    })

def get_segmentation(api, eventName, property, from_date, to_date):
    return api.request(['segmentation'], {
    'event' : eventName,
    'type' : 'general',
    'unit' : 'day',
    'on': 'properties["%s"]' % property,
    'from_date': from_date,
    'to_date': to_date,
    })

def mixpaneldate_to_cyfe(date):
    return date.replace('-', '')

def mixpaneldata_to_cyfe(data, clean=True):
    series = data['data']['series']
    values = data['data']['values']

    # remove unwanted values
    if clean:
        values.pop('', None)
        values.pop('undefined', None)

    fieldnames = ['Date'] + values.keys()
    csvText = ','.join(fieldnames) + '\n'
    for i in range(0, len(series)):
        date = series[i]
        cyfeDate = mixpaneldate_to_cyfe(date)
        csvText = csvText + cyfeDate + ','
        for serie in values.items():
            csvText = csvText + str(serie[1][date]) + ','
        csvText = csvText + '\n'

    return csvText

def mixpaneldata_to_piecyfe(data, clean=True):
    values = data['data']['values']

    # remove unwanted values
    if clean:
        values.pop('', None)
        values.pop('undefined', None)

    csvText = ','.join(values.keys()) + '\n'
    for item in values.items():
        csvText = csvText + str(sum(item[1].values())) + ','
    csvText = csvText + '\n'

    return csvText
