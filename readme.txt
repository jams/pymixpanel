PyMixpanel is a very small web service implemented in python to query Mixpanel for data and format it in csv.
This allows a simple integration with Cyfe dashboard and provides much more capabilities than the current Cyfe Mixpanel widget.

You will have to host this web service on a host with a public url so Cyfe can access it through the Private URL widget.

Usage
-----

This is the most simple request that will returns all specified events:
http://www.yourdomain.com/mixpanel/api/v1.0/event_name/apisecret/apikey

To add a segmentation add a parameter to the url:
http://www.yourdomain.com/mixpanel/api/v1.0/event_name/apisecret/apikey?by=property_name

To display data in a pie chart instead of line curves:
http://www.yourdomain.com/mixpanel/api/v1.0/event_name/apisecret/apikey?charttype=pie

To remove bad data points with property value '' (empty value) or 'undefined':
http://www.yourdomain.com/mixpanel/api/v1.0/event_name/apisecret/apikey?by=property_name&clean=True